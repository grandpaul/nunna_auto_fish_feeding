# nunna_auto_fish_feeding

![fishbowl](docs/img/photo_2018-12-20_17-57-00.jpg)

Sometimes I need to travel foreign countries for few weeks for conferences or
business meetings. But there's no one who can take care of my fishes when I'm
out. So this project makes a simple fish feeder by Arduino.

We design this due to that we don't want to overfeed the fishes which may
kill them. My fish is guppy.

## Contents in this project

We split different sub-components to sub-directories. As following:

* arduino/ the software (sketch) that runs on Arduino Nano or Arduino Pro Mini.
* hardware/ the hardware need to buy or need to print.

## Images

The machine is basically mounted on top of the fishbowl.

![fishbowl 1](docs/img/photo_2018-12-20_17-57-11.jpg)
![fishbowl 2](docs/img/photo_2018-12-20_17-57-15.jpg)

On top is the swirl tube. You need to measure how many food the fish wants
to eat per day. And fill in the tube one by one day and turn the tube a full
circle.

![fishbowl 1](docs/img/photo_2018-12-20_17-56-19.jpg)
![fishbowl 2](docs/img/photo_2018-12-20_17-56-55.jpg)

