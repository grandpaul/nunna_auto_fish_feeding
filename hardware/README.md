# Hardware

## Electronics

Please see breadboard/ sub-directory and using fritzing to open the files.

### BOM

* Arduino Nano or Arduino Pro Mini
* A4988 - stepper motor driver
* NEMA 17 motors
* Breadboard half+ size

## Frame

We use aluminum extrusion to build a frame that suits for tanks.

* 2020 aluminum extrusion 200mm : 4
* 2020 aluminum extrusion 300mm : 8
* 2028 corner angle L brackets connector : 16
* M3 bolt length 8mm : 32
* M3 washers : 32
* M3 European standard hammer head T nuts for 2020: 32

Also you need more bots and nuts for mounting the electronics

* M3 bolt length 6mm : 11
* M3 washers: 9
* M3 European standard hammer head T nuts for 2020: 7
* 625ZZ bearing
* M5 bolt hexagonal head length 40mm : 2
* M5 nuts : 4
* coupling M5 / 5mm : 1

## 3D printed parts

Please see printparts/ sub-directory.
