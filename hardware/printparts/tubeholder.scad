
objHeight=10;
tubeR = 10/2.0;
ringThickness = 2.0;
numOfRing = 12;
spiralD=40;
screwR=5.0/2+0.4;
screwHeadR = 9/2+0.4;

module ring1(tubeR, ringThickness) {
    difference() {
        cylinder(r=tubeR+ringThickness, h=objHeight);
        translate([0,0,-1]) {
            cylinder(r=tubeR, h=objHeight+2, $fn=50);
        }
    }
}

translate([0,tubeR+ringThickness/2,0]) {
    for (i = [0 : numOfRing]) {
        translate([tubeR+ringThickness, (tubeR+ringThickness)+i*2*(tubeR)+i*ringThickness, 0]) {
            ring1(tubeR, ringThickness);
        }
    }
}

for (i = [0 : numOfRing]) {
    translate([tubeR+ringThickness+(tubeR+ringThickness)*cos(30)+(tubeR)*cos(30), (tubeR+ringThickness)+i*2*(tubeR)+i*ringThickness, 0]) {
        ring1(tubeR, ringThickness);
    }
}

translate([spiralD+(tubeR+ringThickness)*cos(30)+(tubeR)*cos(30),tubeR+ringThickness/2,0]) {
    for (i = [0 : numOfRing]) {
        translate([tubeR+ringThickness, (tubeR+ringThickness)+i*2*(tubeR)+i*ringThickness, 0]) {
            ring1(tubeR, ringThickness);
        }
    }
}

translate([spiralD+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30))*2,0,0]) {
    for (i = [0 : numOfRing]) {
        translate([tubeR+ringThickness, (tubeR+ringThickness)+i*2*(tubeR)+i*ringThickness, 0]) {
            ring1(tubeR, ringThickness);
        }
    }
}

difference() {
    cube([spiralD+2*(tubeR+ringThickness)+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30))*2, 2*(tubeR+ringThickness), objHeight]);
    translate([(spiralD+2*(tubeR+ringThickness))/2+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30)),-1,objHeight/2]) {
        rotate([-90,0,0]) {
            cylinder(r=screwR, h=2*(tubeR+ringThickness)+2, $fn=50);
        }
    }
    translate([(spiralD+2*(tubeR+ringThickness))/2+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30)),2*(tubeR+ringThickness)-5,objHeight/2]) {
        rotate([-90,0,0]) {
            rotate([0,0,30]) {
                cylinder(r=screwHeadR, h=2*(tubeR+ringThickness)+2, $fn=6);
            }
        }
    }
}


translate([0,(numOfRing+1)*(2*tubeR+ringThickness),0]) {
    difference() {
        cube([spiralD+2*(tubeR+ringThickness)+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30))*2, 2*(tubeR+ringThickness), objHeight]);
        translate([(spiralD+2*(tubeR+ringThickness))/2+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30)),-1,objHeight/2]) {
            rotate([-90,0,0]) {
                cylinder(r=screwR, h=2*(tubeR+ringThickness)+2, $fn=50);
            }
        }
        translate([(spiralD+2*(tubeR+ringThickness))/2+((tubeR+ringThickness)*cos(30)+(tubeR)*cos(30)),-1,objHeight/2]) {
            rotate([-90,0,0]) {
                rotate([0,0,30]) {
                    cylinder(r=screwHeadR, h=5+1, $fn=6);
                }
            }
        }
    }
}


