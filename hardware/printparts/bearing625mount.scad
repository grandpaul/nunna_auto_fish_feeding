
bearing625R = 16/2.0+0.4;
bearing625HoleR = 9/2.0;
bearing625Thickness = 5+0.4;
bearingHeight = 19.7 + 22.0/2;
screwM3R = 3.0/2+0.4;

translate([-(bearing625R*2 + 2.0*2 + (screwM3R*2 + 2.0*2)*2)/2, 0, 0]) {
    difference() {
        cube([bearing625R*2 + 2.0*2 + (screwM3R*2 + 2.0*2)*2, 20, 2]);
        translate([2+screwM3R,20/2,-1]) {
            cylinder(r=screwM3R, h=4, $fn=50);
        }
        translate([bearing625R*2 + 2.0*2 + (screwM3R*2 + 2.0*2)*2 - 2.0 - screwM3R,20/2,-1]) {
            cylinder(r=screwM3R, h=4, $fn=50);
        }
    }
}
difference() {
    translate([-(bearing625R*2 + 2.0*2)/2, (20-(bearing625Thickness+2.0*2))/2, 0]) {
        cube([bearing625R*2 + 2.0*2, bearing625Thickness+2.0*2, bearingHeight + 2.0]);
    }
    translate([0,(20-bearing625Thickness)/2,bearingHeight]) {
        rotate([-90,0,0]) {
            cylinder(r=bearing625R, h=bearing625Thickness, $fn=80);
        }
    }
    translate([0,-1,bearingHeight]) {
        rotate([-90,0,0]) {
            cylinder(r=bearing625HoleR, h=22, $fn=70);
        }
    }
}


