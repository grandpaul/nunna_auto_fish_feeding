
breadboardWidth = 54.2 + 0.4*2;
breadboardHeight = 82.6 + 0.4*2;
breadboardThick = 9.6 + 0.4;
wallThickness = 2.0;
hookThickness = 1.0;
size2020 = 20;
m3screwR = 3.0/2+0.4;

translate([0,size2020+wallThickness,0]) {
    difference() {
        cube([breadboardWidth + wallThickness*2 + size2020,     breadboardHeight, wallThickness]);
        for (i=[1:5]) {
            hull() {
                translate([size2020/3*1, breadboardHeight*i/6, -1]) {
                    cylinder(r=m3screwR, h=wallThickness+2, $fn=50);
                }
                translate([size2020/3*2, breadboardHeight*i/6, -1]) {
                    cylinder(r=m3screwR, h=wallThickness+2, $fn=50);
                }
            }
        }
    }
    
}

translate([size2020+wallThickness,0,0]) {
    difference() {
        cube([breadboardWidth, breadboardHeight + wallThickness * 2 + size2020, wallThickness]);
        for (i=[1:3]) {
            hull() {
                translate([breadboardWidth*i/4, size2020/3*1, -1]) {
                    cylinder(r=m3screwR, h=wallThickness+2, $fn=50);
                }
                translate([breadboardWidth*i/4, size2020/3*2, -1]) {
                    cylinder(r=m3screwR, h=wallThickness+2, $fn=50);
                }
            }
        }
    }
}

translate([size2020, size2020, 0]) {
    translate([0,-5/2 + breadboardHeight/6*2,0]) {
        cube([wallThickness, 5, wallThickness + breadboardThick]);
    }
    translate([0,-5/2 + breadboardHeight/6*4,0]) {
        cube([wallThickness, 5, wallThickness + breadboardThick]);
    }
    translate([wallThickness+breadboardWidth,-5/2 + breadboardHeight/6*2,0]) {
        cube([wallThickness, 5, wallThickness + breadboardThick]);
    }
    translate([wallThickness+breadboardWidth,-5/2 + breadboardHeight/6*4,0]) {
        cube([wallThickness, 5, wallThickness + breadboardThick]);
    }
    translate([-5/2 + breadboardWidth/6*2, 0, 0]) {
        cube([5, wallThickness, wallThickness+breadboardThick]);
    }
    translate([-5/2 + breadboardWidth/6*4, 0, 0]) {
        cube([5, wallThickness, wallThickness+breadboardThick]);
    }
    translate([-5/2 + breadboardWidth/6*2, wallThickness + breadboardHeight, 0]) {
        cube([5, wallThickness, wallThickness+breadboardThick]);
    }
    translate([-5/2 + breadboardWidth/6*4, wallThickness + breadboardHeight, 0]) {
        cube([5, wallThickness, wallThickness+breadboardThick]);
    }
    translate([wallThickness - hookThickness, -5/2 + breadboardHeight/2, 0]) {
        union() {
            cube([hookThickness, 5, wallThickness + breadboardThick]);
            hull() {
                translate([0,0,wallThickness+breadboardThick]) {
                    cube([hookThickness*2, 5, 0.2]);
                }
                translate([0,0,wallThickness+breadboardThick+hookThickness]) {
                    cube([hookThickness, 5, 0.2]);
                }
            }
        }
    }
    translate([wallThickness+breadboardWidth, -5/2 + breadboardHeight/2, 0]) {
        union() {
            cube([hookThickness, 5, wallThickness + breadboardThick]);
            hull() {
                translate([-hookThickness,0,wallThickness+breadboardThick]) {
                    cube([hookThickness*2, 5, 0.2]);
                }
                translate([0,0,wallThickness+breadboardThick+hookThickness]) {
                    cube([hookThickness, 5, 0.2]);
                }
            }
        }
    }
}


