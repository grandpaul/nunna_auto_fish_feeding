
wallThick = 3;
supportThick=2.5;
holeR=22.0/2;
bearingHeight = 19.7 + 22.0/2;
screwM3R = 3.5/2;
nema17holeDistance = 31;
supportWidth = 28.5;
supportHeight = 20.0;

module support1(w, h, thick) {
    polyhedron(
      points = [ [0,0,0], [0,w,0], [0,0,h], [thick,0,0], [thick, w, 0], [thick, 0, h] ],
      faces = [ [0,1,2], [3,5,4], [0,3,4,1], [0,2,5,3], [1,4,5,2] ]
    );
}

difference() {
    cube([50,51.5,wallThick]);
    translate([50/2, bearingHeight, -1]) {
        cylinder(r=holeR, h=wallThick+2);
    }
    translate([0,51.5,0]) {
        rotate([0,0,45]) {
            cube([7.5/sqrt(2), 7.5/sqrt(2), wallThick*2+2], center=true);
        }
    }
    translate([50,51.5,0]) {
        rotate([0,0,45]) {
            cube([7.5/sqrt(2), 7.5/sqrt(2), wallThick*2+2], center=true);
        }
    }
    for (i=[-1, 2, 1]) {
        for (j=[-1, 2, 1]) {
            translate([50/2+i*nema17holeDistance/2, bearingHeight+j*nema17holeDistance/2, -1]) {
                cylinder(r=screwM3R, h=wallThick+2, $fn=20);
            }
        }
    }
}

difference() {
    cube([50, wallThick, 53.5]);
    translate([0,0,53.5]) {
        rotate([0,45,0]) {
            cube([7.5/sqrt(2), wallThick*2+2, 7.5/sqrt(2)], center=true);
        }
    }
    translate([50,0,53.5]) {
        rotate([0,45,0]) {
            cube([7.5/sqrt(2), wallThick*2+2, 7.5/sqrt(2)], center=true);
        }
    }
    for (i=[-1, 2, 1]) {
        hull() {
            for (j=[0, 1]) {
                translate([50/2+i*nema17holeDistance/2, -1, 10.5+screwM3R+j*nema17holeDistance]) {
                    rotate([-90,0,0]) {
                        cylinder(r=4.5/2, h=wallThick+2, $fn=20);
                    }
                }
            }
        }
    }
}

support1(w=supportWidth, h=supportHeight, thick=supportThick);
translate([50-supportThick,0,0]) {
    support1(w=supportWidth, h=supportHeight, thick=supportThick);
}

