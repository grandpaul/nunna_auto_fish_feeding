#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>

#include <Wire.h>
#include <EEPROM.h>
#include <Arduino_FreeRTOS.h>
#include <semphr.h>

/* Comment the below line out to not have this feature and saves for binary size */
#define CFG_ENABLE_MEMORYFREE 1

#ifdef CFG_ENABLE_MEMORYFREE
#include <MemoryFree.h>
#endif /* CFG_ENABLE_MEMORYFREE */

// defines pins numbers
const int stepPin = 3;
const int dirPin = 4;
const int ledPin = 13;

SemaphoreHandle_t xStepMotorSemaphore = NULL;

/**
 * How many minutes to feed the fish.
 * 86400000L is one day.
 * 120000L is 2 minutes
 */
const unsigned long feedPerMS = 86400000L;
//const unsigned long feedPerMS = 120000L;
unsigned long prevFeedTime = 0;

void setup() {
  // Sets the two pins as Outputs
  pinMode(stepPin,OUTPUT);
  pinMode(dirPin,OUTPUT);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);

  while (!Serial) {
    ;
  }

  digitalWrite(ledPin, LOW);
  digitalWrite(dirPin, LOW);
  digitalWrite(stepPin,LOW);

  if ( xStepMotorSemaphore == NULL) {
    xStepMotorSemaphore = xSemaphoreCreateMutex();
    if (xStepMotorSemaphore != NULL) {
      xSemaphoreGive(xStepMotorSemaphore);
    }
  }
  xTaskCreate(taskFeedFish,
    (const portCHAR *)"FeedFish",
    54,
    NULL,
    2,
    NULL);
  xTaskCreate(taskHandleSerialCommand,
    (const portCHAR *)"SerialCMD",
    142,
    NULL,
    1,
    NULL);

  // We start FreeRTOS scheduler ourselves because I'm using Debian.
  // The arduino package in Debian is too old and does not support
  // initVariant().
  // If you are using latest arduino, you can remove this.
  vTaskStartScheduler();
}

void testRotate() {
  if ( xSemaphoreTake(xStepMotorSemaphore, (TickType_t) 100) != pdTRUE ) {
    return;
  }
  digitalWrite(ledPin, HIGH);
  digitalWrite(dirPin, LOW);
  vTaskDelay(4);
  for (int i=0; i < 20; i++) {
    digitalWrite(stepPin, HIGH);
    vTaskDelay(4);
    digitalWrite(stepPin, LOW);
    vTaskDelay(4);
  }
  digitalWrite(dirPin, HIGH);
  vTaskDelay(4);
  for (int i=0; i < 20; i++) {
    digitalWrite(stepPin, HIGH);
    vTaskDelay(4);
    digitalWrite(stepPin, LOW);
    vTaskDelay(4);
  }
  digitalWrite(dirPin, LOW);
  digitalWrite(ledPin, LOW);
  xSemaphoreGive(xStepMotorSemaphore);
}

unsigned long diffMS(unsigned long oldTime, unsigned long newTime) {
  unsigned long ret;
  if (oldTime <= newTime) {
    return newTime - oldTime;
  }
  ret = (ULONG_MAX - oldTime) + newTime;
  return ret;
}

UBaseType_t taskFeedFishStackWatermark=0;

void taskFeedFish(void *p) {
  unsigned long diffMillis = 0;
  taskFeedFishStackWatermark = uxTaskGetStackHighWaterMark(NULL);
  testRotate();
  while (1) {
    diffMillis = diffMS(prevFeedTime, millis());
    taskFeedFishStackWatermark = uxTaskGetStackHighWaterMark(NULL);
    if (diffMillis < feedPerMS) {
      if (feedPerMS - diffMillis >= 120000L) {
        vTaskDelay(4000); // 1 min
      } else if (feedPerMS - diffMillis >= 150) {
        vTaskDelay(5);
      } else {
        vTaskDelay(1);
      }
      continue;
    }
    prevFeedTime = millis();
    while (xSemaphoreTake(xStepMotorSemaphore, ( TickType_t ) 100 ) != pdTRUE) {
    }
    digitalWrite(ledPin, HIGH);
    digitalWrite(dirPin, LOW);
    vTaskDelay(4);
    for (int i=0; i < 50; i++) {
      digitalWrite(stepPin, HIGH);
      vTaskDelay(4);
      digitalWrite(stepPin, LOW);
      vTaskDelay(4);
    }
    digitalWrite(dirPin, HIGH);
    vTaskDelay(4);
    for (int i=0; i < 250; i++) {
      digitalWrite(stepPin, HIGH);
      vTaskDelay(4);
      digitalWrite(stepPin, LOW);
      vTaskDelay(4);
    }
    digitalWrite(ledPin, LOW);
    digitalWrite(dirPin, LOW);
    xSemaphoreGive(xStepMotorSemaphore);
  }
}

/**
 * Execute a command from Serial console
 * @param cmd the command comes from the Serial port
 */
void runCommand(char *cmd) {
  if (strncmp_P(cmd, PSTR("printenv"), 8)==0) {
    Serial.print(F("prevFeedTime="));
    Serial.println(prevFeedTime);
  }
  else if (strncmp_P(cmd, PSTR("help"), 4)==0) {
    Serial.println(F("printenv - print values of all environment variables"));
    Serial.println(F("setenv - set environment variable"));
    Serial.println(F("eeprom - eeprom operation"));
    Serial.println(F("free - print out free memory size"));
    Serial.println(F("remaintime - print out or set remaintime of feed fish"));
    Serial.println(F("version - show firmware version"));
  }
  else if (strncmp_P(cmd, PSTR("setenv"), 6)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *var = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (var != NULL) {
      if (strncmp_P(var, PSTR("prevFeedTime"), 12)==0) {
        char *arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
        if (arg != NULL) {
          unsigned long data = 0;
          if (sscanf_P(arg, PSTR("%lu"), &data) == 1) {
            prevFeedTime = data;
          }
        }
      }
    }
  }
  else if (strncmp_P(cmd, PSTR("eeprom"), 6)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (subcmd1 == NULL) {
      Serial.println(F("eeprom dump - dump the data of EEPROM"));
      Serial.println(F("eeprom setb <addr(DEC)> <value(HEX)>"));
    }
    else if (strcmp_P(subcmd1, PSTR("dump"))==0) {
      for (int i=0; i<512; i++) {
        int data;
        if (i%16 > 0) {
          Serial.print(' ');
        }
        if (i%16 == 8) {
          Serial.print(' ');
        }
	data = EEPROM.read(i);
	if (data <= 0x0f) {
	  Serial.print(0, HEX);
	}
	Serial.print(data, HEX);
        if (i%16 == 15) {
          Serial.println();
        }
      }
    }
    else if (strcmp_P(subcmd1, PSTR("setb"))==0) {
      char *eAddr;
      eAddr = strtok_rP(NULL, PSTR(" "), &saveptr1);
      if (eAddr != NULL) {
        int eAddrI;
        if (sscanf_P(eAddr, PSTR("%d"), &eAddrI)==1) {
          char *hexValue;
          hexValue = strtok_rP(NULL, PSTR(" "), &saveptr1);
          if (hexValue != NULL) {
            int hexValueI;
            if (sscanf_P(hexValue, PSTR("%x"), &hexValueI)==1) {
              EEPROM.write(eAddrI, hexValueI);
            }
          }
        }
      }
    }
  }
  else if (strncmp_P(cmd, PSTR("remaintime"), 10)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (arg == NULL) {
      unsigned long diffMillis = diffMS(prevFeedTime, millis());
      if (diffMillis >= feedPerMS) {
        Serial.println(F("Feeding now. Wait."));
      } else {
        Serial.print(F("Time remains: "));
        Serial.print(feedPerMS - diffMillis);
        Serial.println(F(" ms"));
      }
    } else {
      unsigned long data=0;
      if (sscanf_P(arg, PSTR("%lu"), &data) == 1) {
        prevFeedTime = millis() - (feedPerMS - data);
      }
    }
  }
  else if (strncmp_P(cmd, PSTR("version"), 7)==0) {
    Serial.print(F("nunna_auto_fish_feeding ("));
    Serial.print(F(__DATE__));
    Serial.print(F(" - "));
    Serial.print(F(__TIME__));
    Serial.println(')');
  }
  else if (strncmp_P(cmd, PSTR("free"), 4)==0) {
    Serial.print(F("Mem: "));
    #ifdef CFG_ENABLE_MEMORYFREE
    Serial.println(freeMemory());
    #else
    Serial.println(F("Not supported"));
    #endif
    Serial.print(F("TaskSerialHandle Stack WaterMark: "));
    Serial.println(uxTaskGetStackHighWaterMark(NULL));
    Serial.print(F("TaskFeedFish Stack WaterMark: "));
    Serial.println(taskFeedFishStackWatermark);
  }
  else {
    Serial.print(F("Unknown command '"));
    Serial.print(cmd);
    Serial.println('\'');
  }
}

char serialBuf[40];
int serialBufLen=0;
void taskHandleSerialCommand(void *p) {
  while (1) {
    for (int i=0; Serial.available() > 0 && i <= 40; i++) {
      int incomingByte = 0;
      incomingByte = Serial.read();
      if (incomingByte == '\r') {
        /* run commands */
        Serial.println();
        runCommand(serialBuf);
        serialBufLen=0;
        serialBuf[0]='\0';
        Serial.print(F("nunna> "));
        Serial.flush();
      }
      else if (incomingByte == '\n') {
        /* ignore */
      }
      else if (incomingByte == 127) {
        if (serialBufLen >= 1) {
          serialBufLen--;
          serialBuf[serialBufLen] = '\0';
          Serial.write(8);
          Serial.flush();
        }
      }
      else {
        if (serialBufLen+1 < (sizeof(serialBuf)/sizeof(serialBuf[0]))) {
          serialBuf[serialBufLen]=incomingByte;
          serialBufLen++;
          serialBuf[serialBufLen]='\0';
        }
        Serial.write(incomingByte);
        Serial.flush();
      }
    }
    vTaskDelay(1);
  }
}

void loop() {
}



